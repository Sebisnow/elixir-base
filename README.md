## Base Image for Elixir deployments

Base image based on `alpine:edge` with Erlang and Elixir installed but not much else.

use this image as base for your production or docker images that should build upon very basic Elixir needs. Otherwise use docker.vhwrz.net/dockerimages/build-elixir to get a more fully featured base container with phoenix and mix available.


Necessary packages to build Erlang:
 - make
 - git
 - gcc
 - g++
 - perl
 - m4 
 - ncurses
 - sed
 - autoconf
 - openssl-dev
 - openjdk8
 - flex
 - (xorg-server-dev)

 commands: 
 
    $ apk add --virtual build-dependencies gcc make git perl m4 ncurses sed autoconf openssl-dev openjdk8 flex (xorg-server-dev)
    $ git clone https://github.com/erlang/otp.git
    $ cd otp
    $ ./otp_build autoconf
    $ ./configure
    $ make
    $ make install
    $ cd ..
    $ rm otp -rf
