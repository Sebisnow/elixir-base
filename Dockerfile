FROM erlang:alpine
MAINTAINER schneidse
# Default to UTF-8 file.encoding
ENV LANG=C.UTF-8

RUN set -x; \
    apk update && \
    apk upgrade && \
    apk --update add bash && \
    rm -rf /var/cache/apk/*